import {GET_PROFILE, PROFILE_ERROR} from "../actions/type";

const initState = {
    profile: null,
    profiles: [],
    repos: [],
    loading: true,
    errors: {}
}

export default function (state = initState, {type, payload}) {
    switch (type) {
        case GET_PROFILE:
            return {
                ...state,
                profile: payload,
                loading: false
            }
        case  PROFILE_ERROR :
            return {
                ...state,
                errors: payload,
                loading: false
            }
        default :
            return Object.assign({}, state)
    }
}