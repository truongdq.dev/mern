import {AUTH_ERROR, LOGOUT, REGISTER_FAILURE, REGISTER_SUCCESS, USER_LOADER} from "../actions/type";


const initState = {
    token: localStorage.getItem('token'),
    isAuthenticated: null,
    loading: true,
    user: null
}


export default function (state = initState, {type, payload}) {
    switch (type) {

        case USER_LOADER :
            return {
                ...state,
                isAuthenticated: true,
                loading: false,
                user: payload
            }

        case REGISTER_SUCCESS :
            localStorage.setItem('token', payload.token);
            return {
                ...state,
                ...payload,
                isAuthenticated: true,
                loading: false
            }
        case AUTH_ERROR :
        case REGISTER_FAILURE :
        case  LOGOUT :
            localStorage.removeItem('token');
            return {
                ...state,
                token: null,
                isAuthenticated: false,
                loading: false
            }
        default :
            return Object.assign({}, state);
    }
}