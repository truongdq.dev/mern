import ajax from "../../config/ajax";
import {AUTH_ERROR, LOGOUT, REGISTER_FAILURE, REGISTER_SUCCESS, USER_LOADER} from "./type";
import {setAlert} from "./alert";


// Login

export const login = ({email, password}) => async dispatch => {
    const body = JSON.stringify({email, password});
    try {
        const response = await ajax.post('auth', body);
        if (response?.data?.errors?.length) {
            let msg = response.data.errors[0].msg;
            return dispatch(setAlert(msg, 'danger'));
        }
        localStorage.setItem('token', response.data.token)
        dispatch(loadUser());
        dispatch(setAlert('Login success', 'success'));
    } catch (e) {
        if (e?.data?.errors?.length) {
            let msg = e.data.errors[0].msg;
            return dispatch(setAlert(msg, 'danger'))
        }
    }
}


// Load user

export const loadUser = () => async dispatch => {
    try {
        const response = await ajax.get('auth');
        if (response.data?.msg) {
            return dispatch({
                type: AUTH_ERROR
            })
        }
        dispatch({
            type: USER_LOADER,
            payload: response.data
        })
    } catch (e) {
        console.log("catch", e)
        localStorage.removeItem('token')
        dispatch({
            type: AUTH_ERROR
        })
    }
}


// Register

export const register = ({name, email, password}) => async dispatch => {

    const body = JSON.stringify({name, email, password});
    try {
        const response = await ajax.post('users', body);
        dispatch({
            type: REGISTER_SUCCESS,
            payload: response.data
        });
        dispatch(setAlert('Register success!', 'success'));

    } catch (e) {
        const errors = e?.data?.errors;
        if (errors?.length) {
            return errors.forEach(error => dispatch(setAlert(error.msg, 'danger')))
        }
        dispatch({
            type: REGISTER_FAILURE
        })
    }
}

export const logout = () => async dispatch => {
    localStorage.removeItem('token')
    dispatch({type: LOGOUT})
}