export const SET_ALERT = "SET_ALERT";

export const REMOVE_ALERT = "REMOVE_ALERT";

export const REGISTER_SUCCESS = "REGISTER_SUCCESS";

export const REGISTER_FAILURE = "REGISTER_FAILURE";

export const USER_LOADER = "USER_LOADER";

export const AUTH_ERROR = "AUTH_ERROR";

export const LOGOUT = "LOGOUT";

export const GET_PROFILE = "GET_PROFILE";

export const PROFILE_ERROR = "PROFILE_ERROR";