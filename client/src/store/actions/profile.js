import ajax from "../../config/ajax";
import {GET_PROFILE, PROFILE_ERROR} from "./type";


export const getCurrentProfile = () => async dispatch => {
    try {
        const response = await ajax.get('/profile/me')
        console.log('profile', response);
        dispatch({
            type: GET_PROFILE,
            payload: response.data
        })
    } catch (e) {
        console.log("error profile", e);
        dispatch({
            type: PROFILE_ERROR,
            payload: {
                msg: e.response?.statusText,
                status: e.response?.status
            }
        })
    }
}