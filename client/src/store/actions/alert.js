import {nanoid} from "nanoid";
import {REMOVE_ALERT, SET_ALERT} from "./type";

export const setAlert = (msg, type, timeout = 3500) => dispatch => {
    const id = nanoid();
    dispatch({
        type: SET_ALERT,
        payload: {
            msg,
            type,
            id
        }
    })
    setTimeout(() => dispatch({type: REMOVE_ALERT, payload: id}), timeout)
}