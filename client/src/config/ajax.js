import axios from "axios";


const ajax = axios.create({
    baseURL: 'http://localhost:5000/api/',
    headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "http://localhost:3000",
    }
})


ajax.interceptors.request.use(async (config) => {

    let token = localStorage.getItem('token');
    if (token) {
        config.headers.common['x-auth-token'] = token
    } else {
        delete config.headers.common['x-auth-token']
    }

    return config
}, async (error) => {
    return Promise.reject(error.response)
})


ajax.interceptors.response.use(async (response) => {
    return response
}, async (error) => {
    return Promise.reject(error.response)
})

export default ajax