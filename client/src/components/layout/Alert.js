import React, {Fragment} from "react";

import {useSelector} from "react-redux";

const Alert = () => {

    const alerts = useSelector(state => state.alert);

    return (
        <Fragment>
            {alerts && alerts.length > 0 && alerts.map(alert => (
                <div key={alert.id} className={`alert alert-${alert.type}`}>
                    {alert.msg}
                </div>
            ))}
        </Fragment>
    )
}

export default Alert

