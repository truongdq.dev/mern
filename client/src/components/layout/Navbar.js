import React, {Fragment} from 'react';
import {Link, useHistory} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {logout} from "../../store/actions/auth";

const Navbar = () => {

    const dispatch = useDispatch()

    const {push} = useHistory()
    const {loading, isAuthenticated} = useSelector(state => state.auth);

    const handleLogout = () => {
        dispatch(logout());
        push('/')
    }

    const authLinks = (
        <ul>
            <li>
                <Link to={"/dashboard"} style={{cursor: "pointer"}}>
                    <i className={'fas fa-user'}/>
                    <span className={'hide-sm'}>DashBoard</span>
                </Link>
            </li>
            <li>
                <div style={{cursor: "pointer"}} onClick={handleLogout}>
                    <i className={'fas fa-sign-out-alt'}/>
                    <span className={'hide-sm'}>Logout</span>
                </div>
            </li>
        </ul>
    )


    const guestLinks = (
        <ul>
            <li>
                <Link to={"/"}>Developers</Link>
            </li>
            <li>
                <Link to={"/register"}>Register</Link>
            </li>
            <li>
                <Link to={"/login"}>Login</Link>
            </li>
        </ul>
    )
    return (
        <nav className="navbar bg-dark">
            <h1>
                <Link to={"/"}><i className="fas fa-code"/>DevConnector</Link>
            </h1>
            {!loading && (<Fragment>{isAuthenticated ? authLinks : guestLinks}</Fragment>)}
        </nav>
    )
}

export default Navbar