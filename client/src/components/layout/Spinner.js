import React, {Fragment} from "react";
import SpinnerIcon from '../../assets/200.gif'

export default () => {
    return (
        <Fragment>
            <img src={SpinnerIcon} alt="spinner"/>
        </Fragment>
    )
}