import React, {Fragment, useEffect, useState} from "react";
import {Link, useHistory} from "react-router-dom";
import {useDispatch, useSelector} from 'react-redux'
import {setAlert} from "../../store/actions/alert";
import {regexEmail} from "../../constants";
import Notiflix from "notiflix";
import {login} from "../../store/actions/auth";

const initFrom = {
    email: "",
    password: ""
}
const Login = () => {
    const history = useHistory();

    const dispatch = useDispatch()

    const [formData, setFormData] = useState(initFrom);

    const authState = useSelector(state => state.auth);

    useEffect(() => {
        if (authState.isAuthenticated) {
            return history.push('/dashboard')
        }
    }, [authState?.isAuthenticated])

    const {email, password} = formData;


    const onChangeForm = e => {
        setFormData(state => ({...state, [e.target.name]: e.target.value}))
    }
    const onSubmit = async e => {
        e.preventDefault();

        if (!regexEmail.test(email)) {
            return dispatch(setAlert('Email not valid', 'danger'))
        }
        if (email !== '' && password !== '') {
            try {
                Notiflix.Loading.circle("Please await....")
                dispatch(login(formData))
                Notiflix.Loading.remove();
                setFormData(initFrom)
            } catch (e) {
                Notiflix.Loading.remove();
            }
        } else {
            dispatch(setAlert('Fields cannot be left blank', 'danger'))
        }
    }
    return (
        <Fragment>
            <h1 className="large text-primary">Sign In</h1>
            <p className="lead">
                <i className="fas fa-user"/>
                Sign into Your Account</p>
            <form className="form" onSubmit={e => onSubmit(e)}>
                <div className="form-group">
                    <input
                        type="email"
                        placeholder="Email Address"
                        name="email"
                        value={email}
                        onChange={e => onChangeForm(e)}
                    />
                </div>
                <div className="form-group">
                    <input
                        type="password"
                        placeholder="Password"
                        name="password"
                        value={password}
                        onChange={e => onChangeForm(e)}
                    />
                </div>
                <input type="submit" className="btn btn-primary" value="Login"/>
            </form>
            <p className="my-1">
                Don't have an account? <Link to={"/register"}>Sign Up</Link>
            </p>
        </Fragment>
    )
}

export default Login