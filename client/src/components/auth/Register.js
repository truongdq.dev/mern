import React, {Fragment, useState} from "react";
import {Link} from "react-router-dom";
import {setAlert} from "../../store/actions/alert";
import {register} from "../../store/actions/auth";
import {useDispatch} from "react-redux";
import Notiflix from "notiflix";

const initForm = {
    name: "",
    email: "",
    password: "",
    confirmPassword: ""
}
const Register = () => {
    const [formData, setFormData] = useState(initForm);

    const dispatch = useDispatch()

    const {name, email, password, confirmPassword} = formData;

    const handleChangeForm = (e) => {
        setFormData(state => ({
            ...state,
            [e.target.name]: e.target.value
        }))
    }

    const onSubmit = async (e) => {
        e.preventDefault();
        if (password !== confirmPassword) {
            return dispatch(setAlert('Password not matching!', 'danger'))
        } else {
            await Notiflix.Loading.circle('Please await...');
            await dispatch(register({name, email, password}));
            await Notiflix.Loading.remove();
            setFormData(initForm)
        }
    }

    return (
        <Fragment>
            <h1 className="large text-primary">Sign Up</h1>
            <p className="lead">
                <i className="fas fa-user"/> Create Your Account</p>
            <form className="form" onSubmit={e => onSubmit(e)}>
                <div className="form-group">
                    <input
                        type="text"
                        placeholder="Name"
                        name="name"
                        value={name}
                        onChange={e => handleChangeForm(e)}/>
                </div>
                <div className="form-group">
                    <input
                        type="email"
                        placeholder="Email Address"
                        name="email"
                        value={email}
                        onChange={e => handleChangeForm(e)}
                    />
                    <small className="form-text"
                    >This site uses Gravatar so if you want a profile image, use a
                        Gravatar email</small
                    >
                </div>
                <div className="form-group">
                    <input
                        type="password"
                        placeholder="Password"
                        name="password"
                        minLength="6"
                        value={password}
                        onChange={e => handleChangeForm(e)}

                    />
                </div>
                <div className="form-group">
                    <input
                        type="password"
                        placeholder="Confirm Password"
                        name="confirmPassword"
                        minLength="6"
                        value={confirmPassword}
                        onChange={e => handleChangeForm(e)}
                    />
                </div>
                <input type="submit" className="btn btn-primary" value="Register"/>
            </form>
            <p className="my-1">
                Already have an account? <Link to={"/login"}>Sign In</Link>
            </p>
        </Fragment>
    )
}

export default Register