import './App.css';
import {Fragment, lazy, Suspense, useEffect} from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import {Provider} from 'react-redux'
import Navbar from "./components/layout/Navbar";
import store from "./store";
import Alert from "./components/layout/Alert";
import {loadUser} from "./store/actions/auth";
import PrivateRoute from "./routing/PrivateRoute";

const LandingScreen = lazy(() => import('./components/layout/Landing'));
const LoginScreen = lazy(() => import('./components/auth/Login'));
const RegisterScreen = lazy(() => import('./components/auth/Register'));
const DashBoardScreen = lazy(() => import('./components/dashboard'))

function App() {

    useEffect(() => {
        store.dispatch(loadUser())
    })

    return (
        <Provider store={store}>
            <Suspense fallback={<div>loading...</div>}>
                <Router>
                    <Fragment>
                        <Navbar/>
                        <Route exact path={'/'} component={LandingScreen}/>
                        <section className={"container"}>
                            <Alert/>
                            <Switch>
                                <Route exact path={'/login'} component={LoginScreen}/>
                                <Route exact path={'/register'} component={RegisterScreen}/>
                                <PrivateRoute exact path={'/dashboard'} Component={DashBoardScreen}/>
                            </Switch>
                        </section>
                    </Fragment>
                </Router>
            </Suspense>
        </Provider>
    );
}

export default App;
