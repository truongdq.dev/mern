import React from "react";
import {Redirect, Route} from "react-router-dom";
import {useSelector} from "react-redux";


const PrivateRoute = ({Component, ...rest}) => {

    const {isAuthenticated, loading} = useSelector(state => state.auth)

    console.log("see", !isAuthenticated, !loading)
    return (
        <Route
            {...rest}
            render={props => !isAuthenticated && !loading ?
                (<Redirect to={'/login'}/>) : (<Component {...props}/>)
            }
        />
    )
}


export default PrivateRoute;