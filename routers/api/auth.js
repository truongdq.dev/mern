const express = require("express");
const router = express.Router();
const auth = require("../../middleware/auth");
const User = require("../../model/User");
const {check, validationResult} = require("express-validator");
const gravatar = require('gravatar');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('config')
// @router  GET api/users
// @desc Test router
// @access Public

router.get("/", auth, async (req, res) => {
  try {
    console.log(req.user);
    const user = await User.findById(req.user.id).select("-password");
    res.json(user);
  } catch (error) {
    console.error(error.message);
    res.status(500).json("Server Error!");
  }
});

// @router  GET api/users
// @desc auth user and get token
// @access Public

router.post("/", [
  check('email', 'Please includes a valid email!').isEmail(),
  check('password', 'Password is required').exists()
], async (req, res) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) return res.status(400).json({errors: errors.array()})

    const {name, email, password} = req.body;

    try {

      const user = await User.findOne({email});
      if (!user) {
        res.status(400).json({
          errors: [{msg: "Invalid Credentials!"}]
        })
      }
      console.log(user)
      const isMatch = await bcrypt.compare(password, user.password);
      if (!isMatch) {
        res.status(400).json({
          errors: [{msg: "Invalid Credentials!"}]
        })
      }


      const payload = {
        user: {
          id: user._id,
          name: user.name,
          email: user.email
        }
      }

      jwt.sign(payload, config.get('jwtSecret'), {expiresIn: 360000}, (err, token) => {
        if (err) throw  err;
        res.json({token})
      })
    } catch (e) {
      console.error(e.message);
      res.status(500).send('Server Error!')
    }
  } catch (error) {
    console.error(error.message);
    res.status(500).json("Server Error!");
  }
});


module.exports = router;
