const express = require("express");
const router = express.Router();
const auth = require("../../middleware/auth");
const Profile = require("../../model/Profile");
const User = require("../../model/User");
const request = require('request');
const config = require('config')
const {check, validationResult} = require("express-validator");

// @router  GET api/profile/me
// @desc get current user profile
// @access Public

router.get("/me", auth, async (req, res) => {
    try {
        const profile = await Profile.findOne({user: req.user.id}).populate(
            "user",
            ["name", "avatar"]
        );
        if (!profile) {
            return res
                .status(400)
                .json({msg: "There is no profile for this user!"});
        }
        return res.json(profile);
    } catch (error) {
        console.error(error.message);
        res.status(500).send("Server Error!");
    }
});

// @router  GET api/profile/me
// @desc create or update user profile
// @access Public

router.post(
    "/",
    [
        auth,
        [
            check("status", "Status is required").not().isEmpty(),
            check("skills", "skills is required").not().isEmpty(),
        ],
    ],
    async (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({errors: errors.array()});
        }

        const {
            company,
            website,
            location,
            bio,
            status,
            githubUsername,
            skills,
            youtube,
            facebook,
            twitter,
            instagram,
            linkedIn,
        } = req.body;

        // build profile
        const profileFields = {};
        profileFields.user = req.user.id;
        if (company) profileFields.company = company;
        if (website) profileFields.website = website;
        if (location) profileFields.location = location;
        if (bio) profileFields.bio = bio;
        if (status) profileFields.status = status;
        if (githubUsername) profileFields.githubUsername = githubUsername;
        if (skills && skills.length) {
            profileFields.skills = skills.split(",").map((skill) => skill.trim());
        }
        // build social object

        profileFields.social = {};
        if (youtube) profileFields.social.youtube = youtube;
        if (twitter) profileFields.social.twitter = twitter;
        if (facebook) profileFields.social.facebook = facebook;
        if (linkedIn) profileFields.social.linkedIn;
        if (instagram) profileFields.social.instagram = instagram;

        let profile = await Profile.findOne({user: req.user.id});
        if (profile) {
            profile = await Profile.findOneAndUpdate(
                {user: req.user.id},
                {$set: profileFields},
                {new: true}
            );
            return res.json(profile);
        }

        // create

        profile = new Profile(profileFields);
        await profile.save();
        res.json(profile);
        try {
        } catch (error) {
            console.error(error.message);
            res.status(500).send("Server Error!");
        }
    }
);

// @router  GET api/profile
// @desc get all profiles
// @access Public

router.get("/", auth, async (req, res) => {
    try {
        const profiles = await Profile.find().populate("user", ["name", "avatar"]);
        res.json(profiles);
    } catch (error) {
        console.error(error.message);
        res.status(500).send("Server Error!");
    }
});

// @router  GET api/profile/user/:user_id
// @desc get one profile
// @access Public

router.get("/user/:user_id", async (req, res) => {
    try {
        const profile = await Profile.findOne({
            user: req.params.user_id,
        }).populate("user", ["name", "avatar"]);

        if (!profile) res.status(400).json({msg: "Profile not found!"});
        res.json(profile);
    } catch (error) {
        if (error.kind == "ObjectId") {
            return res.status(400).json({msg: "Profile not found"});
        }

        res.status(500).send("Server Error!");
    }
});

// @router  DELETE api/profile
// @desc delete profile, user & posts
// @access Public

router.delete("/", auth, async (req, res) => {
    try {
        // todo remove posts

        // remove profile
        await Profile.findOneAndRemove({
            user: req.user.id,
        });

        // remove user
        await User.findOneAndRemove({_id: req.user.id});

        res.status(200).json({msg: "User deleted"});
    } catch (error) {
        if (error.kind == "ObjectId") {
            return res.status(400).json({msg: "Profile not found"});
        }

        res.status(500).send("Server Error!");
    }
});

// @router  PUT api/profile/experience
// @desc add profile experience
// @access Public

router.put(
    "/experience",
    [
        auth,
        [
            check("title", "Title is required").not().isEmpty(),
            check("company", "Company is required").not().isEmpty(),
            check("from", "From is required").not().isEmpty(),
        ],
    ],
    async (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({errors: errors.array()});
        }

        const {title, company, location, from, to, current, description} =
            req.body;

        const newExp = {
            title,
            company,
            location,
            from,
            to,
            current,
            description,
        };

        try {
            const profile = await Profile.findOne({user: req.user.id});
            if (!profile) return res.status(400).json({msg: "User not found!"});
            profile.experience.unshift(newExp);
            await profile.save();

            res.json(profile);
        } catch (error) {
            console.error(error.message);
            res.status(500).json("Server Error");
        }
    }
);

// @router  DELETE api/profile/experience/:exp_id
// @desc delete profile experience
// @access Public

router.delete("/experience/:exp_id", auth, async (req, res) => {
    try {
        const profile = await Profile.findOne({user: req.user.id});

        // remove index
        let checkExist = profile.experience.filter(
            (i) => i._id != req.params.exp_id
        );

        if (!profile.experience.length || !checkExist.length)
            return res.status(204).json({msg: "Not found experience!"});

        profile.experience = profile.experience.filter(
            (i) => i._id != req.params.exp_id
        );
        await profile.save();

        res.json(profile);
    } catch (error) {
        console.error(error.message);
        res.status(500).send("Server Error!");
    }
});

// @router  PUT api/profile/education
// @desc add profile education
// @access Public

router.put(
    "/education",
    [
        auth,
        [
            check("school", "School is required").not().isEmpty(),
            check("degree", "Degree is required").not().isEmpty(),
            check("fieldOfStudy", "FieldOfStudy is required").not().isEmpty(),
            check("from", "from is required").not().isEmpty(),
        ],
    ],
    async (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({errors: errors.array()});
        }

        const {school, degree, fieldOfStudy, from, to, current, description} =
            req.body;

        const newEdu = {
            school,
            degree,
            fieldOfStudy,
            from,
            to,
            current,
            description,
        };

        try {
            const profile = await Profile.findOne({user: req.user.id});
            if (!profile) return res.status(400).json({msg: "User not found!"});
            profile.education.unshift(newEdu);
            await profile.save();

            res.json(profile);
        } catch (error) {
            console.error(error.message);
            res.status(500).json("Server Error");
        }
    }
);

// @router  DELETE api/profile/education/:exp_id
// @desc delete profile education
// @access Public

router.delete("/education/:edu_id", auth, async (req, res) => {
    try {
        const profile = await Profile.findOne({user: req.user.id});

        let checkEducation = profile.education.filter(
            (item) => item._id == req.params.edu_id
        );
        if (!profile.education.length || !checkEducation.length)
            return res.status(204).json("Education not found");
        profile.education = profile.education.filter(
            (item) => item._id != req.params.edu_id
        );

        await profile.save();

        res.json(profile);
    } catch (error) {
        console.error(error.message);
        res.status(500).send("Server Error!");
    }
});

// @router  GET api/profile/github/:username
// @desc  Get user repo from github
// @access Public


router.get('/github/:username', async (req, res) => {
    try {
        const options = {
            uri: `https://api.github.com/users/${req.params.username}/repos?per_page=5&sort=created:asc&
            client_id=${config.get('githubClientId')}&client_secret=${config.get('githubSecret')}`,
            method : "GET",
            headers : {
                'user-agent' : 'node.js'
            }
        }
        request(options,(error,response,body) => {
            if(error) console.error(error);
            if(response.statusCode !== 200){
                res.status(404).json({msg : 'No github profile found!'})
            }
            res.json(JSON.parse(body))
        })

    } catch (e) {
        console.log(e.message)
        res.status(500).json('Server Error!')
    }
})


module.exports = router;
