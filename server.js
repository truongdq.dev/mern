const express = require("express");
const {connectDb} = require("./config/db");
const cors = require('cors')
const app = express();
app.use(cors())
app.use(express.json({extended: false}));
app.get("/", (req, res) => {
    res.send("API RUNNING........");
});

app.use("/api/users", require("./routers/api/users"));
app.use("/api/posts", require("./routers/api/posts"));
app.use("/api/auth", require("./routers/api/auth"));
app.use("/api/profile", require("./routers/api/profile"));

const PORT = process.env.PORT || 5000;

connectDb().then(() => {
    app.listen(PORT, () => console.log("Server started on port ", +PORT));
}).catch(e => {
    console.error(e.message)
});

